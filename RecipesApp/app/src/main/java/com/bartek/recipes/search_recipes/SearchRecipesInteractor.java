package com.bartek.recipes.search_recipes;

import com.bartek.recipes.async_task.GetRecipesAsyncTask;


import java.util.List;

/**
 * Created by Bartek on 17.01.2018.
 */

public interface SearchRecipesInteractor {

    void searchRecipes(String keyword, GetRecipesAsyncTask.GetRecipesAsyncTaskCallback getRecipesAsyncTaskCallback);


}
