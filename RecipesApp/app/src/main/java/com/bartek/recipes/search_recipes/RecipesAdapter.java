package com.bartek.recipes.search_recipes;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bartek.recipes.R;
import com.bartek.recipes.constant.StringExtra;
import com.bartek.recipes.recipe_details.RecipeDetailsActivity;
import com.bartek.recipes.web_service.dto.Recipe;

import java.util.List;

/**
 * Created by Bartek on 17.01.2018.
 */

public class RecipesAdapter extends ArrayAdapter {
    Context context;
    List<Recipe> data;


    public RecipesAdapter(Context context, List<Recipe> data) {
        super(context, R.layout.listview_row_recipes, data);

        this.context = context;
        this.data = data;

    }


    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        convertView = inflater.inflate(R.layout.listview_row_recipes, parent, false);
        TextView mTitleTV = (TextView) convertView.findViewById(R.id.lrc_title_textview);

        convertView.setId(position);
        mTitleTV.setText(data.get(position).getTitle());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, RecipeDetailsActivity.class);
                intent.putExtra(StringExtra.RECIPE_DETAILS_ACTIVITY_ID_RECIPE_EXTRA, data.get(v.getId()).getRecipe_id());
                context.startActivity(intent);
            }
        });


        return convertView;
    }




}

