package com.bartek.recipes.recipe_details;

import com.bartek.recipes.async_task.DownLoadImageAsyncTask;
import com.bartek.recipes.constant.API_key;
import com.bartek.recipes.web_service.dto.Recipe;
import com.bartek.recipes.web_service.get_recipe.params.GetRecipeRequest;
import com.bartek.recipes.web_service.operation.Operation;

import java.io.IOException;

/**
 * Created by Bartek on 21.01.2018.
 */

public class RecipeDetailsInteractorImpl implements RecipeDetailsInteractor {


    @Override
    public Recipe getRecipe(String idRecipe) {



            GetRecipeRequest request = new GetRecipeRequest();
            request.setKey(API_key.API_KEY);
            request.setrId(idRecipe);
            Recipe recipe = Operation.getRecipe(request).getRecipe();
          return recipe;

    }

    @Override
    public void getImage(String url, DownLoadImageAsyncTask.DownLoadImageAsyncTaskCallback callback)  {
       DownLoadImageAsyncTask downLoadImageAsyncTask=new DownLoadImageAsyncTask();
        downLoadImageAsyncTask.downLoadImageAsyncTaskCallback=callback;
        downLoadImageAsyncTask.execute(url);
    }
}
