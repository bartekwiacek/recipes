package com.bartek.recipes.constant;

/**
 * Created by Bartek on 17.01.2018.
 */

public class URI {
    public static String SEARCH_RECIPES="http://food2fork.com/api/search";
    public static String SEARCH_RECIPE="http://food2fork.com/api/get";
}
