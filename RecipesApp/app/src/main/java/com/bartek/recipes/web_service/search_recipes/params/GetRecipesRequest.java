package com.bartek.recipes.web_service.search_recipes.params;

/**
 * Created by Bartek on 19.01.2018.
 */

public class GetRecipesRequest {
    private String key;
    private String q;
    private String sort;
    private String page;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getQ() {
        return q;
    }

    public void setQ(String q) {
        this.q = q;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }
}
