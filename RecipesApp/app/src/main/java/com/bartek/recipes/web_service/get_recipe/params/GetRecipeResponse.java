package com.bartek.recipes.web_service.get_recipe.params;

import com.bartek.recipes.web_service.dto.Recipe;

import java.util.List;

/**
 * Created by Bartek on 19.01.2018.
 */

public class GetRecipeResponse {

   Recipe recipe;

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }
}
