package com.bartek.recipes.async_task;

import android.os.AsyncTask;

import com.bartek.recipes.web_service.dto.Recipe;
import com.bartek.recipes.web_service.operation.Operation;
import com.bartek.recipes.web_service.search_recipes.params.GetRecipesRequest;
import com.bartek.recipes.web_service.search_recipes.params.GetRecipesResponse;

import java.util.List;

/**
 * Created by Bartek on 20.12.2017.
 */

public class GetRecipesAsyncTask extends AsyncTask<GetRecipesRequest, Void, List<Recipe>> {


    GetRecipesAsyncTaskCallback getRecipesAsyncTaskCallback;


    public void setCallback(GetRecipesAsyncTaskCallback getRecipesAsyncTaskCallback) {
        this.getRecipesAsyncTaskCallback = getRecipesAsyncTaskCallback;
    }

    @Override
    protected List<Recipe> doInBackground(GetRecipesRequest... params) {
        try {


            GetRecipesResponse response= Operation.getRecipes(params[0]);
            List<Recipe> profileJsonList=response.getRecipes();

            return profileJsonList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }


    }

    @Override
    protected void onPostExecute(List<Recipe> profileJsonList) {
        super.onPostExecute(profileJsonList);
        if (profileJsonList != null) {

            if (!isCancelled())
                getRecipesAsyncTaskCallback.onSuccessGetRecipes(profileJsonList);
        } else {
            if (!isCancelled())
                getRecipesAsyncTaskCallback.onFaildGetRecipes();
        }

    }


    public interface GetRecipesAsyncTaskCallback {
        void onSuccessGetRecipes(List<Recipe> recipesList);

        void onFaildGetRecipes();
    }
}
