package com.bartek.recipes.search_recipes;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bartek.recipes.R;

import com.bartek.recipes.web_service.dto.Recipe;

import java.util.List;

public class SearchRecipesActivity extends AppCompatActivity implements SearchRecipesView {
    private ImageButton mSearchIBtn;
    private ImageButton mClearIBtn;
    private EditText mSearchKeywordET;
    private ListView mRecipesLV;
    private ProgressBar mProgressBar;
    private RecipesAdapter mRecipesAdapter;
    private SearchRecipesPresenter mSearchRecipesPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_recipes);


        mSearchIBtn = (ImageButton) findViewById(R.id.am_search_imagebutton);
        mClearIBtn = (ImageButton) findViewById(R.id.am_clear_imagebutton);
        mSearchKeywordET = (EditText) findViewById(R.id.am_search_keyword_edittext);
        mRecipesLV = (ListView) findViewById(R.id.am_recipies_listview);
        mProgressBar = (ProgressBar) findViewById(R.id.am_progressbar);

        mSearchRecipesPresenter = new SearchRecipesPresenterImpl(this,new SearchRecipesInteractorImpl());


        mSearchIBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearchRecipesPresenter.onSearchRecipeItemClicked(mSearchKeywordET.getText().toString());
            }
        });
        mClearIBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearchKeywordET.setText("");
                mRecipesAdapter.clear();
            }
        });

        mSearchKeywordET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    mSearchRecipesPresenter.onSearchRecipeItemClicked(mSearchKeywordET.getText().toString());
                }
                return false;
            }
        });


    }

    @Override
    public void showLoading() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void setItems(List<Recipe> items) {
        mRecipesAdapter = new RecipesAdapter(this, items);
       // mRecipesLV.removeAllViews();
        mRecipesLV.setAdapter(mRecipesAdapter);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public Context getContext() {
        return this;
    }
}
