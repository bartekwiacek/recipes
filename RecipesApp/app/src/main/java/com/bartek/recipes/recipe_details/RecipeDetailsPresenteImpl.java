package com.bartek.recipes.recipe_details;

import android.graphics.Bitmap;

import com.bartek.recipes.async_task.DownLoadImageAsyncTask;
import com.bartek.recipes.constant.API_key;
import com.bartek.recipes.constant.StringExtra;
import com.bartek.recipes.web_service.dto.Recipe;
import com.bartek.recipes.web_service.get_recipe.params.GetRecipeRequest;
import com.bartek.recipes.web_service.operation.Operation;

import java.io.IOException;

/**
 * Created by Bartek on 21.01.2018.
 */

public class RecipeDetailsPresenteImpl implements RecipeDetailsPresenter, DownLoadImageAsyncTask.DownLoadImageAsyncTaskCallback {
    private RecipeDetailsView recipeDetailsView;
    private RecipeDetailsInteractor recipeDetailsInteractor;
    private Integer idRecipe;
    private Recipe recipe;

    public RecipeDetailsPresenteImpl(RecipeDetailsView recipeDetailsView,RecipeDetailsInteractor recipeDetailsInteractor) {
        this.recipeDetailsView = recipeDetailsView;
        this.recipeDetailsInteractor=recipeDetailsInteractor;
        init();


    }


    @Override
    public void init() {
        String id = recipeDetailsView.getStringExtra(StringExtra.RECIPE_DETAILS_ACTIVITY_ID_RECIPE_EXTRA);
        recipe=recipeDetailsInteractor.getRecipe(id);
        recipeDetailsView.setDetails(recipe);
        recipeDetailsInteractor.getImage(recipe.getImage_url(),this);

    }

    @Override
    public void onSuccessDownloadImage(Bitmap bitmap) {
        recipeDetailsView.setImageView(bitmap);
    }

    @Override
    public void onFaildDownloadImage() {

    }
}
