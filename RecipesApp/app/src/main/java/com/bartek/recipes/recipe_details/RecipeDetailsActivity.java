package com.bartek.recipes.recipe_details;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bartek.recipes.R;
import com.bartek.recipes.web_service.dto.Recipe;

public class RecipeDetailsActivity extends AppCompatActivity implements RecipeDetailsView {

    private ImageView mImageView;
    private TextView mTitleTV;
    private TextView mSocialRankTV;
    private TextView mURLTV;
    private TextView mPublisherTV;
    private TextView mIngredientsTV;
    private RecipeDetailsPresenter mRecipeDetailsPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_details);
        mTitleTV = (TextView) findViewById(R.id.ard_title_textview);
        mImageView = (ImageView) findViewById(R.id.ard_image_view);
        mPublisherTV = (TextView) findViewById(R.id.ard_publisher_textview);
        mURLTV = (TextView) findViewById(R.id.ard_url_textview);
        mSocialRankTV = (TextView) findViewById(R.id.ard_social_rank_textview);
        mIngredientsTV = (TextView) findViewById(R.id.ard_ingredients_textview);


        mRecipeDetailsPresenter = new RecipeDetailsPresenteImpl(this, new RecipeDetailsInteractorImpl());


    }


    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setDetails(Recipe recipe) {

        mTitleTV.setText(recipe.getTitle());
        mSocialRankTV.setText("" +  String.format("%.2f", recipe.getSocial_rank()));
        mPublisherTV.setText("" + recipe.getPublisher());
        mURLTV.setText("" + recipe.getF2f_url());
        mURLTV.setPaintFlags(mURLTV.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mURLTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(mURLTV.getText().toString()));
                startActivity(i);
            }
        });
        String ingredients = "";
        for (String ingredient : recipe.getIngredients()) {
            ingredients += (ingredient + "\n");
        }
        mIngredientsTV.setText(ingredients);

    }

    @Override
    public void setImageView(Bitmap bitmap) {
        mImageView.setImageBitmap(bitmap);
    }

    @Override
    public String getStringExtra(String stringExtra) {
        if (getIntent().hasExtra(stringExtra)) {
            String url = getIntent().getStringExtra(stringExtra);
            return url;
        } else return null;
    }

    @Override
    public Context getContext() {
        return this;
    }
}
