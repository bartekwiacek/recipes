package com.bartek.recipes.search_recipes;

import android.content.Context;

import com.bartek.recipes.web_service.dto.Recipe;

import java.util.List;

/**
 * Created by Bartek on 17.01.2018.
 */

public interface SearchRecipesView {
    void showLoading();

    void hideLoading();

    void setItems(List<Recipe> items);

    void showMessage(String message);

    Context getContext();
}
