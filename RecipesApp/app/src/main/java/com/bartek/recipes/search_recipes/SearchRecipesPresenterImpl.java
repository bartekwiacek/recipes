package com.bartek.recipes.search_recipes;

import com.bartek.recipes.R;
import com.bartek.recipes.async_task.GetRecipesAsyncTask;
import com.bartek.recipes.tools.Internet;
import com.bartek.recipes.web_service.dto.Recipe;

import java.util.List;

/**
 * Created by Bartek on 17.01.2018.
 */

public class SearchRecipesPresenterImpl implements SearchRecipesPresenter ,GetRecipesAsyncTask.GetRecipesAsyncTaskCallback{
private SearchRecipesView searchRecipesView;
    private SearchRecipesInteractor searchRecipesInteractor;

    public SearchRecipesPresenterImpl(SearchRecipesView searchRecipesView, SearchRecipesInteractor searchRecipesInteractor) {
        this.searchRecipesView = searchRecipesView;
        this.searchRecipesInteractor = searchRecipesInteractor;
    }

    @Override
    public void onSearchRecipeItemClicked(String keyword) {
        searchRecipesView.showLoading();
        if(Internet.isOnline(searchRecipesView.getContext()))
        searchRecipesInteractor.searchRecipes(keyword,this);
        else
            searchRecipesView.showMessage(searchRecipesView.getContext().getResources().getString(R.string.no_internet_connection_error));



    }

    @Override
    public void onSuccessGetRecipes(List<Recipe> recipesList) {
        searchRecipesView.setItems(recipesList);
        searchRecipesView.hideLoading();
    }

    @Override
    public void onFaildGetRecipes() {

    }
}
