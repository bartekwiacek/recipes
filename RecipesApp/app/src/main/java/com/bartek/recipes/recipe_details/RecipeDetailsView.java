package com.bartek.recipes.recipe_details;

import android.content.Context;
import android.graphics.Bitmap;

import com.bartek.recipes.web_service.dto.Recipe;

/**
 * Created by Bartek on 21.01.2018.
 */

public interface RecipeDetailsView {
    void showMessage(String message);

    void setDetails(Recipe recipe);

    void setImageView(Bitmap bitmap);

    String getStringExtra(String stringExtra);

    Context getContext();
}
