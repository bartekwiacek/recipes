package com.bartek.recipes.recipe_details;

import com.bartek.recipes.async_task.DownLoadImageAsyncTask;
import com.bartek.recipes.web_service.dto.Recipe;

import java.io.IOException;

/**
 * Created by Bartek on 21.01.2018.
 */

public interface RecipeDetailsInteractor {
    Recipe getRecipe(String idRecipe);
    void getImage(String url ,DownLoadImageAsyncTask.DownLoadImageAsyncTaskCallback callback);
}
