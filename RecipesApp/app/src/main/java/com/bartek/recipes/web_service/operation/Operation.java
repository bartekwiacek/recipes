package com.bartek.recipes.web_service.operation;

import android.os.StrictMode;

import com.bartek.recipes.constant.URI;
import com.bartek.recipes.web_service.dto.Recipe;
import com.bartek.recipes.web_service.get_recipe.params.GetRecipeRequest;
import com.bartek.recipes.web_service.get_recipe.params.GetRecipeResponse;
import com.bartek.recipes.web_service.search_recipes.params.GetRecipesRequest;
import com.bartek.recipes.web_service.search_recipes.params.GetRecipesResponse;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bartek on 19.01.2018.
 */

public class Operation {
    private static RestTemplate setConnection() {


        RestTemplate rest = new RestTemplate();

        List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
        messageConverters.add(new MappingJackson2HttpMessageConverter());
        messageConverters.add(new StringHttpMessageConverter());
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        rest.setMessageConverters(messageConverters);
        rest.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
        ((HttpComponentsClientHttpRequestFactory) rest.getRequestFactory()).setReadTimeout(1000 * 60);
        ((HttpComponentsClientHttpRequestFactory) rest.getRequestFactory()).setConnectTimeout(1000 * 60);

        return rest;
    }

    public static GetRecipesResponse getRecipes(GetRecipesRequest request) {
        try {
            String URL = URI.SEARCH_RECIPES;
            RestTemplate rest = setConnection();
            UriComponentsBuilder builder = UriComponentsBuilder
                    .fromUriString(URL)
                    .queryParam("q", request.getQ())
                    .queryParam("key", request.getKey());

            String json = rest.postForObject(builder.build().toString(), request, String.class);
            Gson g = new Gson();
            Type type = new TypeToken<GetRecipesResponse>() {
            }.getType();
            GetRecipesResponse response = g.fromJson(json, type);

            return response;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    public static GetRecipeResponse getRecipe(GetRecipeRequest request) {
        try {
            String URL = URI.SEARCH_RECIPE;
            RestTemplate rest = setConnection();
            UriComponentsBuilder builder = UriComponentsBuilder
                    .fromUriString(URL)
                    .queryParam("rId", request.getrId())
                    .queryParam("key", request.getKey());

            String json = rest.postForObject(builder.build().toString(), request, String.class);
            Gson g = new Gson();
            Type type = new TypeToken<GetRecipeResponse>() {
            }.getType();

           GetRecipeResponse response= g.fromJson(json, type);

            return response;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
