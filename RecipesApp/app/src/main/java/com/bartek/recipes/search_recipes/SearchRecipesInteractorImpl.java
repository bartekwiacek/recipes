package com.bartek.recipes.search_recipes;

import com.bartek.recipes.async_task.GetRecipesAsyncTask;
import com.bartek.recipes.constant.API_key;
import com.bartek.recipes.web_service.search_recipes.params.GetRecipesRequest;

/**
 * Created by Bartek on 17.01.2018.
 */

public class SearchRecipesInteractorImpl implements  SearchRecipesInteractor {
    @Override
    public void searchRecipes(String keyword, GetRecipesAsyncTask.GetRecipesAsyncTaskCallback getRecipesAsyncTaskCallback) {
        GetRecipesRequest request=new GetRecipesRequest();
        request.setKey(API_key.API_KEY);
        request.setQ(keyword);

        GetRecipesAsyncTask task=new GetRecipesAsyncTask();
        task.setCallback(getRecipesAsyncTaskCallback);
        task.execute(request);

    }
}
