package com.bartek.recipes.async_task;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.InputStream;
import java.net.URL;

public class DownLoadImageAsyncTask extends AsyncTask<String, Void, Bitmap> {


    public DownLoadImageAsyncTaskCallback downLoadImageAsyncTaskCallback;

    protected Bitmap doInBackground(String... urls) {
        String urlOfImage = urls[0];
        Bitmap logo = null;
        try {
            InputStream is = new URL(urlOfImage).openStream();

            logo = BitmapFactory.decodeStream(is);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return logo;
    }

    protected void onPostExecute(Bitmap result) {
        if (downLoadImageAsyncTaskCallback != null) {
            if (result != null)
                downLoadImageAsyncTaskCallback.onSuccessDownloadImage(result);
            else downLoadImageAsyncTaskCallback.onFaildDownloadImage();
        }


    }

    public interface DownLoadImageAsyncTaskCallback {
        void onSuccessDownloadImage(Bitmap bitmap);

        void onFaildDownloadImage();
    }
}

