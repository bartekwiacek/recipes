package com.bartek.recipes.search_recipes;

/**
 * Created by Bartek on 17.01.2018.
 */

public interface SearchRecipesPresenter {

    void onSearchRecipeItemClicked(String keyword);


}
