package com.bartek.recipes.web_service.get_recipe.params;

/**
 * Created by Bartek on 19.01.2018.
 */

public class GetRecipeRequest {
    private String key;
    private String rId;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getrId() {
        return rId;
    }

    public void setrId(String rId) {
        this.rId = rId;
    }
}
