package com.bartek.recipes.web_service.search_recipes.params;

import com.bartek.recipes.web_service.dto.Recipe;

import java.util.List;

/**
 * Created by Bartek on 19.01.2018.
 */

public class GetRecipesResponse {
    private String count;
    private List<Recipe> recipes;

    public GetRecipesResponse() {

    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public List<Recipe> getRecipes() {
        return recipes;
    }

    public void setRecipes(List<Recipe> recipes) {
        this.recipes = recipes;
    }
}
